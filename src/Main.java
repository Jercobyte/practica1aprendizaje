
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private static BufferedReader bf; 
    private static KnowledgeBase kb;
    
    private static void initialize(){
        bf = new BufferedReader(new InputStreamReader(System.in));
        kb = IOManager.readExcelData("alarm.c.xlsx");
    }
    
    public static void probabilityCLI() throws IOException{
        System.out.println("Choose variable to analize: ");
        System.out.println("0) Catechol");
        System.out.println("1) HR");
        System.out.println("2) HREKG");
        System.out.println("3) HRSat");
        System.out.println("4) HRBP");
        System.out.println("5) InsuffAnesth");
        System.out.print("> ");
        int variable = Integer.parseInt(bf.readLine());
        boolean hasC=false;
        if(variable>=0 && variable<=5){
            System.out.println("Select the level of that variable over which you want to compute probability");
            if(variable == Case.InsuffAnesth || variable == Case.Catechol){
                System.out.println("a");
                System.out.println("b");
            }else{
                hasC = true;
                System.out.println("a");
                System.out.println("b");
                System.out.println("c");
            }
            System.out.print("> ");
            String value = bf.readLine();
            if(value.equals("a") || value.equals("b") || (value.equals("c") && hasC)){
                System.out.println("The probability of "+Case.getVariableName(variable)+" being '"+value+"' is "+kb.probability(variable,value));
            }else{
                System.out.println("Wrong parameters");
            }
        }else{
            System.out.println("Wrong parameters");
        }
        System.out.println("");
    }
    
    public static void diagnosisCLI() throws IOException{
        System.out.println("Choose symptoms: ");
        System.out.println("0) Catechol");
        System.out.println("1) HR");
        System.out.println("2) HREKG");
        System.out.println("3) HRSat");
        System.out.println("4) HRBP");
        System.out.print("> ");
        int variable = Integer.parseInt(bf.readLine());
        boolean hasC=false;
        if(variable>=0 && variable<=4){
            System.out.println("Select the level of "+Case.getVariableName(variable));
            if(variable == Case.Catechol){
                System.out.println("a");
                System.out.println("b");
            }else{
                hasC = true;
                System.out.println("a");
                System.out.println("b");
                System.out.println("c");
            }
            System.out.print("> ");
            String value = bf.readLine();
            if(value.equals("a") || value.equals("b") || (value.equals("c") && hasC)){
                System.out.println("The diagnosis of InsuffAnesth being 'a' assuming "+Case.getVariableName(variable)+" being '"+value+"' is "+kb.bayes(Case.InsuffAnesth, Case.InsuffAnesth_a, variable, value));
                System.out.println("The diagnosis of InsuffAnesth being 'b' assuming "+Case.getVariableName(variable)+" being '"+value+"' is "+kb.bayes(Case.InsuffAnesth, Case.InsuffAnesth_b, variable, value));
            }else{
                System.out.println("Wrong parameters");
            }
        }else{
            System.out.println("Wrong parameters");
        }
        System.out.println("");
    }
    
    public static void predictionCLI() throws IOException{
        System.out.println("Choose status of InsuffAnesth: ");
        System.out.println("a");
        System.out.println("b");
        System.out.print("> ");
        String insuffAnesthvalue = bf.readLine();
        if(insuffAnesthvalue.equals("a") || insuffAnesthvalue.equals("b")){
            System.out.println("Choose symptom over which you want to compute its probability to appear: ");
            System.out.println("0) Catechol");
            System.out.println("1) HR");
            System.out.println("2) HREKG");
            System.out.println("3) HRSat");
            System.out.println("4) HRBP");
            System.out.print("> ");
            int variable = Integer.parseInt(bf.readLine());
            boolean hasC=false;
            System.out.println("Select the level of "+Case.getVariableName(variable));
            if(variable == Case.Catechol){
                System.out.println("0) a");
                System.out.println("1) b");
            }else{
                hasC = true;
                System.out.println("a");
                System.out.println("b");
                System.out.println("c");
            }
            System.out.print("> ");
            String value = bf.readLine();
            if(value.equals("a") || value.equals("b") || (value.equals("c") && hasC)){
                System.out.println("It's predicted to appear the symptom "+Case.getVariableName(variable)+" and value '"+value+"' with a probability of "+kb.bayes(variable, value, Case.InsuffAnesth, insuffAnesthvalue)+" assuming InsuffAnesth '"+insuffAnesthvalue+"'");
            }else{
                System.out.println("Wrong parameters");
            }
        }else{
            System.out.println("Wrong parameters");
        }
        System.out.println("");
    }
    
    public static void sampleQuestions(){
        //Sample questions
        System.out.println("# of cases with Catechol 'a': "+kb.getNumberOfCasesWith(Case.Catechol,Case.Catechol_a));
        System.out.println("# of cases with HR 'c': "+kb.getNumberOfCasesWith(Case.HR,Case.HR_c));
        System.out.println("# of cases with InsuffAnesth 'b': "+kb.getNumberOfCasesWith(Case.InsuffAnesth,Case.InsuffAnesth_b));
        
        //Probability sample
        System.out.println("Probability of InsuffAnesth 'a': "+kb.probability(Case.InsuffAnesth,Case.InsuffAnesth_a));
        
        //Diagnosis sample
        System.out.println("Diagnosis: Probability of InsuffAnesth 'b' assuming HRSat 'b': "+kb.bayes(Case.InsuffAnesth,Case.InsuffAnesth_b,Case.HRSat,Case.HRSat_b));
        
        //Prediction sample
        System.out.println("Prediction: Probability of HRBP 'a' assuming InsuffAnesth 'b': "+kb.bayes(Case.HRBP,Case.HRBP_a,Case.InsuffAnesth,Case.InsuffAnesth_b));
        System.out.println("");
    }
    
    public static void main(String[] args) throws IOException{
        initialize();
        int action = 3;
        do{
            System.out.println("Alarm for patient monitoring. Simple Knowledge Based System. Select operation:");
            System.out.println("0) Compute variable probability");
            System.out.println("1) Make diagnosis from symptoms");
            System.out.println("2) Make predictions according to facts");
            System.out.println("3) Show some sample actions");
            System.out.println("4) Print current knowledge base from Excel file");
            System.out.println("5) Exit");
            System.out.print("> ");
            action = Integer.parseInt(bf.readLine());
            System.out.println("");
            switch(action){
                case 0:
                    probabilityCLI();
                    break;
                case 1:
                    diagnosisCLI();
                    break;
                case 2:
                    predictionCLI();
                    break;
                case 3:
                    sampleQuestions();
                    break;
                case 4:
                    System.out.println(kb);
                    break;
                case 5: 
                    System.out.println("Bye.");
                    break;
                default:
                    System.out.println("Wrong option value. Please choose a correct one.");
            }
        }while(action!=5);
    }
}
