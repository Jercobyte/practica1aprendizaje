
import java.util.ArrayList;
import java.util.List;

public class Case {
    private List<String> values;
    
    public static final int Catechol = 0;
    public static final String Catechol_a = "a";
    public static final String Catechol_b = "b";
    
    
    public static final int HR = 1;
    public static final String HR_a = "a";
    public static final String HR_b = "b";
    public static final String HR_c= "c";
    
    
    public static final int HREKG = 2;
    public static final String HREKG_a = "a";
    public static final String HREKG_b = "b";
    public static final String HREKG_c = "c";
    
    
    public static final int HRSat = 3;
    public static final String HRSat_a = "a";
    public static final String HRSat_b = "b";
    public static final String HRSat_c = "c";
    
    public static final int HRBP = 4;
    public static final String HRBP_a = "a";
    public static final String HRBP_b = "b";
    public static final String HRBP_c = "c";
    
    public static final int InsuffAnesth = 5;
    public static final String InsuffAnesth_a = "a";
    public static final String InsuffAnesth_b = "b";

    public Case(String Catechol, String HR, String HREKG, String HRSat, String HRBP, String InsuffAnesth) {
        this.values=new ArrayList<>();
        this.values.add(Catechol);
        this.values.add(HR);
        this.values.add(HREKG);
        this.values.add(HRSat);
        this.values.add(HRBP);
        this.values.add(InsuffAnesth);
    }

    public String getValue(int variable) {
        return values.get(variable);
    }

    @Override
    public String toString() {
        return "Case{" + "Catechol=" + values.get(0) + ", HR=" + values.get(1) + ", HREKG=" + values.get(2) + ", HRSat=" + values.get(3) + ", HRBP=" + values.get(4) + ", InsuffAnesth=" + values.get(5) + '}';
    }
    
    public static String getVariableName(int variable){
        String res = "";
        switch(variable){
            case Case.Catechol:
                res = "Catechol";
                break;
            case Case.HR:
                res = "HR";
                break;
            case Case.HREKG:
                res = "HREKG";
                break;
            case Case.HRSat:
                res = "HRSat";
                break;
            case Case.HRBP:
                res = "HRBP";
                break;
            case Case.InsuffAnesth:
                res = "InsuffAnesth";
                break;
        }
        return res;
    }
}
