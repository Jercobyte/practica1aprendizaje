import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class IOManager {
    public static KnowledgeBase readExcelData(String fileName){
        KnowledgeBase kb = new KnowledgeBase();
        try
        {
            FileInputStream file = new FileInputStream(new File(fileName));
 
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            for(int i=1;i<sheet.getLastRowNum();i++){
                String hypovolemia = sheet.getRow(i).getCell(0).getStringCellValue();
                String lVFailure = sheet.getRow(i).getCell(1).getStringCellValue();
                String lVEDVolume = sheet.getRow(i).getCell(2).getStringCellValue();
                String strokeVolume = sheet.getRow(i).getCell(3).getStringCellValue();
                String cVP = sheet.getRow(i).getCell(4).getStringCellValue();
                String pCWP = sheet.getRow(i).getCell(5).getStringCellValue();
                kb.addCase(new Case(hypovolemia,lVFailure,lVEDVolume,strokeVolume,cVP,pCWP));
            }
            file.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return kb;
    }
}
