import java.util.ArrayList;
import java.util.List;

public class KnowledgeBase {
    private List<Case> kb;
    
    public KnowledgeBase(){
        this.kb = new ArrayList<Case>();
    }
    
    public void addCase(Case c){
        this.kb.add(c);
    }
    
    public int getKnowledgeBaseSize(){
        return kb.size();
    }
    
    public int getNumberOfCasesWith(int variable, String value){
        return getNumberOfCasesWith(new int[]{variable},new String[]{value});
    }

    public int getNumberOfCasesWith(int[] vars, String[] values){
        int amount = 0;
        int numArgs = Math.min(vars.length, values.length);

        // For case
        for (Case aKb : kb) {
            int validConditions = 0;
            for (int j = 0; j < numArgs; j++) {
                if (aKb.getValue(vars[j]).equals(values[j])) {
                    validConditions++;
                }
            }

            if (validConditions == numArgs) {
                amount++;
            }
        }
        return amount;
    }
    
    public double probability(int variable, String value){
        return (double) getNumberOfCasesWith(variable,value)/kb.size();
    }
    

    public double bayes(int variable1, String target, int variable2, String symptom){
        int a = getNumberOfCasesWith(
                new int[]{variable1, variable2},
                new String[]{target, symptom});
        int b = getNumberOfCasesWith(variable2,symptom);
        return (double) ((double)a/(double)b);
    }
    
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Knowledge base: \n");
        for(Case c : kb){
            sb.append(c).append("\n");
        }
        return sb.toString();
    }
}
